﻿using BitventureWorkbench.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BitventureWorkbench
{
    public partial class Form1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnHolidays_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormViewHolidays form = new FormViewHolidays();
            form.MdiParent = this;
            form.Show();
        }
    }
}
