﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitventureWorkbench.Globals
{
    public class HolidayDto
    {
        public int ID { get; set; }

        public string Description { get; set; }

        public DateTime Date{ get; set; }

        public bool CanOperate { get; set; }
    }
}
