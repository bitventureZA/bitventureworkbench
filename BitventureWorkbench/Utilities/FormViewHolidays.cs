﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BitventureWorkbench.Globals;

namespace BitventureWorkbench.Utilities
{
    public partial class FormViewHolidays : DevExpress.XtraEditors.XtraForm
    {
        HolidayDto _holidayData = new HolidayDto();

        public FormViewHolidays()
        {
            InitializeComponent();
        }

        private void FormViewHolidays_Load(object sender, EventArgs e)
        {
            Result results = LoadData();

            if (results.resultCode == 0)
            {
                gridViewHolidays.BestFitColumns();
            }

            else
            {
                MessageBox.Show(results.resultMessage);
                this.Close();
            }
        }



        private Result LoadData()
        {
            try
            {
                this.virtualPublicHolidayTableAdapter.Fill(this.easyDebitDataSet.virtualPublicHoliday);
                return CodeExecution.Succeeded();
            }
            catch (Exception ex)
            {
                return CodeExecution.Failed(string.Concat("Failed to refresh data: ", ex.Message.ToString()));
            }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            int currentRowHandle = gridViewHolidays.GetVisibleRowHandle(0);
            while (currentRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                EasyDebitDataSet.virtualPublicHolidayRow dr =
                    (EasyDebitDataSet.virtualPublicHolidayRow)gridViewHolidays.GetDataRow(currentRowHandle);

                dr.IsSelected = true;
                dr.AcceptChanges();

                currentRowHandle = gridViewHolidays.GetNextVisibleRow(currentRowHandle);
            }
        }

        private void btnUnselectAll_Click(object sender, EventArgs e)
        {
            int currentRowHandle = gridViewHolidays.GetVisibleRowHandle(0);
            while (currentRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                EasyDebitDataSet.virtualPublicHolidayRow dr =
                    (EasyDebitDataSet.virtualPublicHolidayRow)gridViewHolidays.GetDataRow(currentRowHandle);

                dr.IsSelected = false;
                dr.AcceptChanges();

                currentRowHandle = gridViewHolidays.GetNextVisibleRow(currentRowHandle);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddEditHoliday form = new AddEditHoliday();
            form.SetToAdd();
            form.ShowDialog();

            Result results = LoadData();
            if (results.resultCode != 0)
                MessageBox.Show(results.resultMessage);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // TODO: Delete Holiday
        }

        private void gridViewHolidays_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                EasyDebitDataSet.virtualPublicHolidayRow dr = (EasyDebitDataSet.virtualPublicHolidayRow)gridViewHolidays.GetFocusedDataRow();
                _holidayData.ID = dr.ID;
                _holidayData.Description = dr.Description;
                _holidayData.CanOperate = dr.CanOperate;
                _holidayData.Date = dr.Date;
            }
            catch (Exception)
            {
                _holidayData.ID = 0;
            }
        }

        private void gridViewHolidays_DoubleClick(object sender, EventArgs e)
        {
            if (_holidayData.ID > 0)
            {
                // Open form.
                AddEditHoliday form = new AddEditHoliday();
                form.SetToEdit(_holidayData);
                form.ShowDialog();

                Result results = LoadData();
                if (results.resultCode != 0)
                    MessageBox.Show(results.resultMessage);
            }

            else
            {
                MessageBox.Show("Please select a holiday before continuing.");
            }
        }
    }
}